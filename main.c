#include <iostream>
#include "htmlparser.h"

int main() {
    std::string dom("<!doctype html><html><body><p id=\"aa\">This is para grapth</p></body></html>");
    HTMLDocument *doc = simple_load_dom(dom);

    //HTMLElement *ele = doc->getElementById("aa");
    std::vector<HTMLElement*> eles = doc->getElementByTagName("p");
    if (eles.size() > 0) {
        std::cout << "OK" << std::endl;
    }

    return 0;
}
