#ifndef HTMLPARSER_H
#define HTMLPARSER_H

#include <stdio.h>
#include <iostream>
#include <cstring>
#include <vector>
#include <map>
#include <set>

#if __cplusplus <= 199711L
    #if linux
        #include <tr1/memory>
    #else
        #include <memory>
    #endif
    using std::tr1::shared_ptr;
#else
    #include <memory>
    using std::shared_ptr;
#endif

class HTMLElement {
    public:
        HTMLElement() {}
        HTMLElement(std::string tag) : tagName(tag), id("") {
            m_attributes["id"] = "";
            m_attributes["class"] = "";
            m_attributes["name"] = "";
        }
        HTMLElement(std::shared_ptr<HTMLElement> parent) {
            m_parent = parent.get();
        }
        void appendChild(std::shared_ptr<HTMLElement> newNode) {
            newNode->m_parent = this;
            m_childrens.emplace_back(std::move(newNode));
        }
        bool hasClass(const std::string className) const {
            const auto it = m_attributes.find("class");
            if (it == m_attributes.end()) {
                return false;
            }
            const auto& names = m_attributes.at("class");
            return names.find(className) != std::string::npos;
        }
        std::string tagName = "";
        void addClass(const std::string className) {
            if (!hasClass(className)) {
                const auto it = m_attributes.find("class");
                if (it == m_attributes.end()) {
                    m_attributes["class"] = className;
                } else {
                    const auto& names = m_attributes.at("class");
                    m_attributes["class"] = names + " " + className;
                }
            }
        }
        void setAttribute(const std::string name, std::string value) {
            m_attributes[name] = value;
            if (name == "id") {
                id = value;
            }

        }
        std::string getAttribute(const std::string name) {
            const auto it = m_attributes.find(name);
            if (it == m_attributes.end()) {
                return "";
            }
            return it->second;
        }
        void removeAttribute(std::string name) {
            m_attributes.erase(name);
        }
        bool hasAttribute(const std::string name) const {
            return m_attributes.find(name) != m_attributes.end();
        }
        HTMLElement *children(size_t index) {
            if (m_childrens.size() == 0) {
                return nullptr;
            }
            if (index < 0) {
                index = m_childrens.size() + index;
            }
            if (index >= m_childrens.size()) {
                return nullptr;
            }
            return m_childrens[index].get();
        }
        HTMLElement *firstChild() {
            return children(0);
        }
        HTMLElement *lastChild() {
            return children(-1);
        }
        HTMLElement *nextSibling() {
            if (m_parent == NULL) {
                return nullptr;
            }
            size_t index = 0;
            size_t count = m_parent->m_childrens.size();
            while (index < count && this != m_parent->m_childrens[index].get()) {
                ++index;
            }
            if (++index >= count) {
                return nullptr;
            }
            return m_parent->m_childrens[index].get();
        }
        HTMLElement *prevSibling() {
            if (m_parent == NULL) {
                return nullptr;
            }
            size_t index = 0;
            size_t count = m_parent->m_childrens.size();
            while (index < count && this != m_parent->m_childrens[index].get()) {
                ++index;
            }
            if (--index < 0) {
                return nullptr;
            }
            return m_parent->m_childrens[index].get();
        }
        void removeChild(size_t index) {
            if (index < 0) {
                index = m_childrens.size() + index;
            }
            if (index < m_childrens.size()) {
                m_childrens.erase(m_childrens.begin() + index);
            }
        }
        HTMLElement *getElementById(const std::string &id) {
            std::vector<HTMLElement*> queue(1, this);
            while (queue.size()) {
                std::vector<HTMLElement*> next;
                for(auto& node : queue) {
                    if (node->id == id) {
                        return node;
                    }
                    for(auto& i : node->m_childrens) {
                        next.push_back(i.get());
                    }
                }
                queue = std::move(next);
            }
            return nullptr;
        }
        std::vector<HTMLElement*> getElementByClassName(const std::string className) {
            std::vector<HTMLElement*> retVal;
            std::vector<HTMLElement*> queue(1, this);
            while (queue.size()) {
                std::vector<HTMLElement*> next;
                for(auto& node : queue) {
                    if (node->hasClass(className)) {
                        retVal.push_back(node);
                    }
                    for(auto& i : node->m_childrens) {
                        next.push_back(i.get());
                    }
                }
                queue = std::move(next);
            }
            return retVal;
        }
        std::vector<HTMLElement*> getElementByTagName(const std::string tagName) {
            std::vector<HTMLElement*> retVal;
            std::vector<HTMLElement*> queue(1, this);
            while (queue.size()) {
                std::vector<HTMLElement*> next;
                for(auto& node : queue) {
                    if (node->tagName == tagName) {
                        retVal.push_back(node);
                    }
                    for(auto& i : node->m_childrens) {
                        next.push_back(i.get());
                    }
                }
                queue = std::move(next);
            }
            return retVal;
        }
        std::vector<HTMLElement*> getElementByName(const std::string name) {
            std::vector<HTMLElement*> retVal;
            std::vector<HTMLElement*> queue(1, this);
            while (queue.size()) {
                std::vector<HTMLElement*> next;
                for(auto& node : queue) {
                    if (node->getAttribute("name") == name) {
                        retVal.push_back(node);
                    }
                    for(auto& i : node->m_childrens) {
                        next.push_back(i.get());
                    }
                }
                queue = std::move(next);
            }
            return retVal;
        }
        HTMLElement *parent() {
            return m_parent;
        }
        void parent(std::shared_ptr<HTMLElement> parent) {
            m_parent = parent.get();
        }
        std::vector<HTMLElement*> querySelectorAll(const std::string selector) {
            std::vector<HTMLElement*> retVal;
            std::vector<HTMLElement*> queue(1, this);
            while (queue.size()) {
                std::vector<HTMLElement*> next;
                for(auto& node : queue) {
                    if (node->matchesSelector(selector)) {
                        retVal.push_back(node);
                    }
                    for(auto& i : node->m_childrens) {
                        next.push_back(i.get());
                    }
                }
                queue = std::move(next);
            }
            return retVal;
        }
        HTMLElement *querySelector(const std::string selector) {
            std::vector<HTMLElement*> queue(1, this);
            while (queue.size()) {
                std::vector<HTMLElement*> next;
                for(auto& node : queue) {
                    if (node->matchesSelector(selector)) {
                        return node;
                    }
                    for(auto& i : node->m_childrens) {
                        next.push_back(i.get());
                    }
                }
                queue = next;
            }
            return nullptr;
        }
        std::vector<HTMLElement*> find(const std::string selector) {
            std::vector<HTMLElement*> retVal;

            return retVal;
        }
        std::string id = "";
        std::string innerHTML = "";
        std::string innerText = "";
    private:
        bool matchesSelector(const std::string selector) const {
            std::string temp = selector.substr(1);
            return tagName == selector || m_attributes.at("id") == temp && selector[0] == '#' || hasClass(temp) && selector[0] == '.';
        }
        std::vector<std::shared_ptr<HTMLElement>> m_childrens = {};
        std::map<std::string, std::string> m_attributes;
        HTMLElement *m_parent;
};

class HTMLDocument {
    public:
        HTMLDocument() {
            m_root = new HTMLElement("html");
        }
        HTMLDocument(const std::shared_ptr<HTMLElement> &root) {
            m_root = root.get();
        }
        void appendChild(std::shared_ptr<HTMLElement> newNode) {
            m_root->appendChild(newNode);
        }
        HTMLElement *getElementById(const std::string &id) {
            return m_root->getElementById(id);
        }
        std::vector<HTMLElement*> getElementByClassName(const std::string className) {
            return m_root->getElementByClassName(className);
        }
        std::vector<HTMLElement*> getElementByTagName(const std::string name) {
            return m_root->getElementByTagName(name);
        }
        std::vector<HTMLElement*> getElementByName(const std::string name) {
            return m_root->getElementByName(name);
        }
        HTMLElement *children(size_t index) {
            return m_root->children(index);
        }
        HTMLElement *firstChild() {
            return m_root->firstChild();
        }
        HTMLElement *lastChild() {
            return m_root->lastChild();
        }
        void removeChild(size_t index) {
            m_root->removeChild(index);
        }
        std::vector<HTMLElement*> querySelectorAll(const std::string selector) {
            return m_root->querySelectorAll(selector);
        }
        HTMLElement *querySelector(const std::string selector) {
            return m_root->querySelector(selector);
        }
        HTMLElement *nextSibling() {
            return m_root->nextSibling();
        }
        HTMLElement *prevSibling() {
            return m_root->prevSibling();
        }
        std::vector<HTMLElement*> find(const std::string selector) {
            return m_root->find(selector);
        }
    private:
        HTMLElement *m_root;
};

class HTMLParser {
    public:
        HTMLParser() { }
        shared_ptr<HTMLDocument> parse(const std::string data) {
            stream = data.data();
            length = data.size();
            root.reset(new HTMLElement());
            size_t index = 0;
            char ch;
            while (index < length) {
                ch = stream[index];
                if (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') {
                    index++;
                } else if (ch == '<') {
                    index = element(index, root);
                } else {
                    break;
                }
            }
            return shared_ptr<HTMLDocument>(new HTMLDocument(root));
        }
    private:
        size_t element(size_t index, shared_ptr<HTMLElement> &ele) {
            char ch, split = 0;
            enum state_t {
                ELEMENT_TAG,
                ELEMENT_ATTR,
                ELEMENT_VALUE,
                ELEMENT_END
            };
            state_t state = ELEMENT_TAG;
            std::string attrs;
            while(index < length) {
                ch = stream[index + 1];
                if(ch == '!') {
                    if (strncmp(stream + index, "<!--", 4) == 0) {
                        return skip(index + 2, "-->");
                    } else {
                        return skip(index + 2, '>');
                    }
                } else if (ch == '/') {
                    return skip(index,'>');
                } else if (ch == '?') {
                    return skip(index, "?>");
                }
                shared_ptr<HTMLElement> self(new HTMLElement(ele));
                index++;
                while(index < length) {
                    switch(state) {
                        case ELEMENT_TAG: {
                            ch = stream[index];
                            if (ch == ' ' || ch == '\n' || ch == '\r' || ch == '\t') {
                                if (!self->tagName.empty()) {
                                    state = ELEMENT_ATTR;
                                }
                                index++;
                            } else if (ch == '/') {
                                attributes(attrs, self);
                                ele->appendChild(self);
                                return skip(index, '>');
                            } else if (ch == '>') {
                                if (closing_tags.find(self->tagName) != closing_tags.end()) {
                                    ele->appendChild(self);
                                    return ++index;
                                }
                                state = ELEMENT_VALUE;
                                index++;
                            } else {
                                self->tagName.append(1, ch);
                                index++;
                            }
                        } break;
                        case ELEMENT_ATTR: {
                            ch = stream[index];
                            if (ch == '>') {
                                if (stream[index - 1] == '/') {
                                    attrs.erase(attrs.size() - 1);
                                    attributes(attrs, self);
                                    ele->appendChild(self);
                                    return ++index;
                                } else if (closing_tags.find(self->tagName) != closing_tags.end()) {
                                    attributes(attrs, self);
                                    ele->appendChild(self);
                                    return ++index;
                                }
                                state = ELEMENT_VALUE;
                                index++;
                            } else {
                                attrs.append(1, ch);
                                index++;
                            }
                        } break;
                        case ELEMENT_VALUE: {
                            if(self->tagName == "script" || self->tagName == "noscript" || self->tagName == "style") {
                                std::string close = "</" + self->tagName + ">";
                                size_t pre = index;
                                index = skip(index, close.c_str());
                                if (index > (pre + close.size())) {
                                    self->innerHTML.append(stream + pre, index - pre - close.size());
                                }
                                attributes(attrs, self);
                                ele->appendChild(self);
                                return index;
                            }
                            ch = stream[index];
                            if (ch == '<') {
                                if (stream[index + 1] == '/') {
                                    state = ELEMENT_END;
                                } else {
                                    index = element(index, self);
                                }
                            } else {
                                self->innerHTML.append(1,ch);
                                index++;
                            }
                        } break;
                        case ELEMENT_END: {
                            index += 2;
                            std::string selfname = self->tagName + ">";
                            if (strncmp(stream + index, selfname.c_str(), selfname.size())) {
                                std::cout << selfname << " > " << stream + index << std::endl;
                                size_t pre = index;
                                index = skip(index, '>');
                                std::string value;
                                if (index > (pre + 1)) {
                                    value.append(stream + pre, index - pre - 1);
                                } else {
                                    value.append(stream + pre, index - pre);
                                }
                                HTMLElement *parent = self->parent();
                                while (parent) {
                                    std::cout << "Loop parent <" << parent->tagName << ">" << std::endl;
                                    if (parent->tagName == value) {
                                        std::cerr << "WARN: element not closed <" << self->tagName << "> " << std::endl;
                                        return pre - 2;
                                    }
                                    parent = parent->parent();
                                }
                                std::cerr << "WARN: unexpected closed element </" << value << "> for <" << self->tagName << ">" << std::endl;
                                state = ELEMENT_VALUE;
                            } else {
                                attributes(attrs, self);
                                ele->appendChild(self);
                                return skip(index, '>');
                            }
                        } break;
                    }
                }
            }
            return index;
        }
        void attributes(const std::string attr, shared_ptr<HTMLElement> &ele) {
            size_t index = 0;
            std::string k, v;
            char split = ' ', ch;
            bool quota = false;
            enum att_t {
                ATTR_KEY,
                ATTR_VALUE,
                ATTR_END
            };
            att_t state = ATTR_KEY;
            while (index < attr.size()) {
                ch = attr.at(index);

                switch(state) {
                    case ATTR_KEY: {
                        if (ch == '\t' || ch == '\r' || ch == '\n') {
                            index++;
                            continue;
                        } else if (ch == '\'' || ch == '"') {
                            std::cerr << "WARN : attribute unexpected " << ch << std::endl;
                        } else if (ch == ' ') {
                            if (!k.empty()) {
                                ele->setAttribute(k, v);
                                k.clear();
                            }
                        } else if (ch == '=') {
                            state = ATTR_VALUE;
                        } else {
                            k.append(1, ch);
                        }
                    } break;
                    case ATTR_VALUE: {
                        if (ch == '\t' || ch == '\r' || ch == '\n' || ch == ' ') {
                            if (!k.empty()) {
                                ele->setAttribute(k, v);
                                k.clear();
                            }
                            state = ATTR_KEY;
                        } else if (ch == '\'' || ch == '"') {
                            split = ch;
                            quota = true;
                            state = ATTR_END;
                        } else {
                            v.append(1, ch);
                            quota = false;
                            state = ATTR_END;
                        }
                    } break;
                    case ATTR_END: {
                        if ((quota && split == ch) ||  (!quota && (ch == '\t' || ch == '\n' || ch == '\r' || ch == ' '))) {
                            ele->setAttribute(k, v);
                            k.clear();
                            v.clear();
                            state = ATTR_KEY;
                        } else {
                            v.append(attr.c_str() + index, 1);
                        }
                    } break;
                }
                index++;
            }
            if (!k.empty()) {
                ele->setAttribute(k, v);
            }
            if (!ele->innerText.empty()) {
                ele->innerText.erase(0, ele->innerText.find_first_not_of(" "));
                ele->innerText.erase(ele->innerText.find_last_not_of(" ") + 1);
            }
            if (!ele->innerHTML.empty()) {
                ele->innerHTML.erase(0, ele->innerHTML.find_first_not_of(" "));
                ele->innerHTML.erase(ele->innerHTML.find_last_not_of(" ") + 1);
            }
        }
        size_t skip(size_t index, const char *data) {
            while(index < length) {
                if (strncmp(stream + index, data, strlen(data)) == 0) {
                    return index + strlen(data);
                }
                index++;
            }
            return index;
        }
        size_t skip(size_t index, const char ch) {
            while(index < length) {
                if (stream[index] == ch) {
                    return ++index;
                }
                index++;
            }
            return index;
        }
        const char *stream;
        size_t length;
        std::set<std::string> closing_tags { "br", "hr", "img", "input", "link", "meta", "area", "base", "col", "command", "embed", "keygen", "param", "source", "track", "wbr" };
        shared_ptr<HTMLElement> root;
};


HTMLDocument *simple_load_dom(const std::string html) {
    if (html.size() == 0) {
        return nullptr;
    }
    HTMLParser parser;
    shared_ptr<HTMLDocument> doc = parser.parse(html);
    return doc.get();
}

#endif
