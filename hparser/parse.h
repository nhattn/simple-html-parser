#ifndef PARSER_H
#define PARSER_H

#include <string>
#include <vector>
#include "dom.h"

std::vector<attribute*> parse_attributes(std::string);

char *parse_tag(char *, std::vector<attribute*> &, std::string &);

struct endTag *create_end_tag(char *);

struct startTag *create_start_tag(char *);

Dom *parse_html(std::string);

#endif
