#include <iostream>
#include "parse.h"

int main() {
    std::string html("<html><head><script></script></head><body><a href='http://google.com'>redirect to</a><div><a href='http://twitter.com'></a></div><div><a href='http://facebook.com'></a></div><p>The test html</p></body></html>");
    Dom *d = parse_html(html);
    std::cout << d->get_name() << std::endl;
    std::cout<< d->get_children().size() << std::endl;  //2
    std::cout << d->get_children()[0]->get_name() << std::endl;  //head
    std::cout << d->get_children()[0]->get_children().size() << std::endl; // 1
    std::cout << d->get_children()[0]->get_children()[0]->get_name() << std::endl; //script
    std::cout << d->get_children()[1]->get_children()[0]->get_children()[0]->find("a")[0]->get_link() << std::endl;  // '/'
    std::cout << d->find("img")[1]->get_img() << std::endl;    // '/images/logooo.png'
    std::cout << d->get_children()[1]->get_children()[0]->get_attrs()[0]->value << std::endl;
    std::cout << d->find("\"logo\"")[0]->find("a")[0]->get_link() << std::endl;  //  /
    delete(d);
    return 0;
}
