#ifndef DOM_H
#define DOM_H

#include "tag_object.h"

class Dom {
        startTag *self;
        Dom *parent;
        std::vector<Dom*> childrens;
    public:
        Dom(startTag*, Dom*);
        std::vector<attribute*> get_attrs();
        std::string get_name();
        std::string get_text();
        Dom *get_parent();
        void set_parent(Dom*);
        std::vector<Dom*> get_children();
        void add_child(Dom*);
        bool is_tag(std::string);
        bool is_name(std::string);
        bool is_id(std::string);
        bool is_href(std::string);
        bool has_attribute(std::string);
        bool has_link();
        bool has_img();
        std::string get_link();
        std::string get_img();
        std::vector<Dom*> find(std::string);
        std::vector<Dom*> get_by_name(std::string);
        std::vector<Dom*> get_by_tag(std::string);
        std::vector<Dom*> get_by_class(std::string);
        Dom *get_by_id(std::string);
        bool has_class(std::string);
        std::string get_attribute(std::string);
        ~Dom();
};
#endif
