#include <iostream>
#include <map>
#include <stack>
#include <string.h>
#include <unordered_map>
#include "parse.h"

std::map<std::string, bool> m {
    { "area", true },
    { "button", true },
    { "br", true },
    { "col", true },
    { "embed", true },
    { "hr", true },
    { "img", true },
    { "input", true },
    { "meta", true },
    { "wbr", true }
};

std::vector<attribute*> parse_attributes(std::string str) {
    std::vector<attribute*> attrs;
    std::vector<std::string> words;
    std::string sep1 = "=", sep2 = " ";
    int start = 0, end1 = 0, end2 = 0;
    while ((str.find(sep1) != std::string::npos) || (str.find(sep2) != std::string::npos)) {
        end1 = str.find(sep1);
        end2 = str.find(sep2);
        if(end1 != std::string::npos && end2 != std::string::npos) {
            if(end1 < end2) {
                words.push_back(str.substr(0, end1));
                str = str.substr(end1 + 1);
            } else {
                words.push_back(str.substr(0, end2));
                str = str.substr(end2 + 1);
            }
        } else if (end1 != std::string::npos) {
            words.push_back(str.substr(0, end1));
            str = str.substr(end1 + 1);
        } else if (end2 != std::string::npos) {
            words.push_back(str.substr(0, end2));
            str = str.substr(end2 + 1);
        } else {
            std::cout << "Parsing error..." << std::endl;
        }
    }
    words.push_back(str.substr(start));
    for(int i = 0; i < words.size() - 1; i += 2) {
        attribute *attr = new attribute(words[i], words[i + 1]);
        attrs.push_back(attr);
    }
    return attrs;
}

char *parse_tag(char *tag, std::vector<attribute*> &attrs, std::string &text) {
    if(*tag == '/') {
        tag++;
        std::string tagName;
        while(*tag != '>') {
            tagName.append(1, *tag);
            tag++;
        }
        return (char *)tagName.c_str();
    } else {
        std::string toFind = "=", strtag(tag), tagName;
        if (strtag.find(toFind) != std::string::npos) {
            int i = 0;
            while (strtag[i] != ' ') {
                tagName.append(1, strtag[i]);
                i++;
            }
            if(*(strtag.end() - 1) != '>') {
                int sep;
                if ((sep = strtag.find(">")) != std::string::npos) {
                    std::string terminate = ">";
                    int size = strtag.find(terminate) - (tagName.size() + 1);
                    std::string to_extract_attrs = strtag.substr(i, size);
                    attrs = parse_attributes(to_extract_attrs);
                    std::string extracted_text = strtag.substr(strtag.find(">") + 1);
                    text = extracted_text;
                    return (char *)tagName.c_str();
                } else {
                    std::cout << "Parsing error";
                    return (char *)tagName.c_str();
                }
            } else {
                std::string terminate = ">";
                int size = strtag.find(terminate) - (tagName.size() + 1);
                std::string to_extract_attrs = strtag.substr(i, size);
                attrs = parse_attributes(to_extract_attrs);
                return (char *)tagName.c_str();
            }
        } else {
            int i = 0;
            while (strtag[i] != '>') {
                tagName.append(1, strtag[i]);
                i++;
            }
            if(*(strtag.end() - 1) != '>') {
                std::string extracted_text = strtag.substr(strtag.find(">") + 1);
                text = extracted_text;
            }
            return (char *)tagName.c_str();
        }
    }
}

struct endTag *create_end_tag(char *tag) {
    char *tagName;
    std::vector<attribute*> attrs;
    std::string text;
    try {
        tagName = parse_tag(tag, attrs, text);
    } catch (std::exception &e) {
        std::cout << "Get tag name exception: " << e.what() << std::endl;
    }
    struct endTag *et;
    if (tagName != NULL) {
        std::string strName(tagName);
        et = new endTag(strName, true, false);
    }
    return et;
}

struct startTag *create_start_tag(char *tag) {
    char *tagName;
    std::vector<attribute*> attrs;
    std::string t;
    try {
        tagName = parse_tag(tag, attrs, t);
    } catch(std::exception &e) {
        std::cout << "Parse start tag exception: " << e.what() << std::endl;
    }
    struct startTag *st;
    if (tagName != NULL) {
        std::string strName(tagName);
        st = new startTag(strName, true, false, attrs);
        st->text = t;
    }
    return st;
}

Dom *parse_html(std::string html) {
    char *html_buf = (char *)html.data();
    char *pch;
    pch = strtok(html_buf, "<");
    std::string current_tag;
    std::stack<Dom*> s;
    Dom *current_dom = NULL, *last_dom = NULL, *result = NULL;
    s.push(last_dom);
    while(pch != NULL) {
        if (*pch == '/') {
            struct endTag *et = create_end_tag(pch);
            last_dom = s.top();
            if (last_dom->get_name() == et->name) {
                result = s.top();
                s.pop();
            }
        } else {
            struct startTag *st = create_start_tag(pch);
            last_dom = s.top();
            current_dom = new Dom(st, last_dom);
            if (last_dom != NULL) {
                last_dom->add_child(current_dom);
            }
            if (m.find(st->name) == m.end()) {
                s.push(current_dom);
            }
        }
        pch = strtok(NULL, "<");
    }
    return result;
}
