#include <vector>
#include <string>
#include <cstring>

struct DOCTYPE {
    std::string name;
    std::string publicid;
    std::string sysid;
    bool force_quirks;
};

struct attribute {
    std::string name;
    std::string value;
    attribute(std::string name, std::string value) : name(name), value(value) {}
};

struct startTag {
    std::string name;
    bool is_start_tag;
    bool is_end_tag;
    std::vector<attribute*> attrs;
    std::string text;
    startTag(std::string name, bool start, bool end, std::vector<attribute*> attrs) : name(name), is_start_tag(start), is_end_tag(end), attrs(attrs){}
};

struct endTag {
    std::string name;
    bool is_end_tag;
    bool is_start_tag;
    endTag(std::string name, bool end, bool start) : name(name), is_end_tag(end), is_start_tag(start) {}
};
