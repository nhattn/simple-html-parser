#include <iostream>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <exception>
#include <vector>
#include "dom.h"

void find_helper(Dom *me, std::vector<Dom*> &result, std::string filter) {
    if (me == NULL) {
        return;
    }
    if (me->is_tag(filter) || me->has_attribute(filter)) {
        result.push_back(me);
    }
    for(int i = 0; i < me->get_children().size(); ++i) {
        find_helper(me->get_children()[i], result, filter);
    }
}

Dom::Dom(startTag *st, Dom *p) {
    self = st;
    parent = p;
}

std::vector<attribute*> Dom::get_attrs() {
    std::vector<attribute*> attrs;
    try {
        attrs = self->attrs;
    } catch(std::exception &e) {
        std::cout << "Exception name: " << e.what();
    }
    return attrs;
}

std::string Dom::get_name() {
    try {
        return self->name;
    } catch(std::exception &e) {
        std::cout << "Exception name: " << e.what();
        return "";
    }
}

std::string Dom::get_text() {
    return self->text;
}

Dom *Dom::get_parent() {
    return parent;
}

void Dom::set_parent(Dom *p) {
    parent = p;
}

std::vector<Dom*> Dom::get_children() {
    std::vector<Dom*> childs;
    try {
        childs = childrens;
    } catch(std::exception &e) {
        std::cout << "Exception name: " << e.what();
    }
    return childs;
}

void Dom::add_child(Dom *d) {
    childrens.push_back(d);
}

bool Dom::is_tag(std::string str) {
    if (self->name == str) {
        return true;
    }
    return false;
}

bool Dom::is_name(std::string n) {
    for(int i = 0; i < self->attrs.size(); i++) {
        if (self->attrs[i]->name == "name" && self->attrs[i]->value == n) {
            return true;
        }
    }
    return false;
}

bool Dom::is_id(std::string id) {
    for(int i = 0; i < self->attrs.size(); i++) {
        if (self->attrs[i]->name == "id" && self->attrs[i]->value == id) {
            return true;
        }
    }
    return false;
}

bool Dom::is_href(std::string href) {
    for(int i = 0; i < self->attrs.size(); i++) {
        if (self->attrs[i]->name == "href" && self->attrs[i]->value == href) {
            return true;
        }
    }
    return false;
}

bool Dom::has_attribute(std::string name) {
    for(int i = 0; i < self->attrs.size(); i++) {
        if (self->attrs[i]->name == name) {
            return true;
        }
    }
    return false;
}

std::string Dom::get_attribute(std::string name) {
    for(int i = 0; i < self->attrs.size(); i++) {
        if (self->attrs[i]->name == name) {
            return self->attrs[i]->value;
        }
    }
    return "";
}

bool Dom::has_link() {
    return self->name == "a";
}

bool Dom::has_img() {
    return self->name == "img";
}

std::string Dom::get_link() {
    if (has_link()) {
        for(int i = 0; i < self->attrs.size(); ++i) {
            if (self->attrs[i]->name == "href") {
                return self->attrs[i]->value;
            }
        }
    }
    return "";
}

std::string Dom::get_img() {
    if (has_img()) {
        for(int i = 0; i < self->attrs.size(); ++i) {
            if (self->attrs[i]->name == "src") {
                return self->attrs[i]->value;
            }
        }
    }
    return "";
}

std::vector<Dom*> Dom::find(std::string filter) {
    std::vector<Dom*> result;
    find_helper(this, result, filter);
    return result;
}

Dom::~Dom() {
    delete(self);
    for(int i = 0; i < self->attrs.size(); ++i) {
        delete(self->attrs[i]);
        std::cout << "attribute deleted" << std::endl;
    }
    for(int i = 0; i < childrens.size(); ++i) {
        delete(childrens[i]);
    }
    std::cout << "The dom object is being deleted" << std::endl;
}

bool Dom::has_class(std::string className) {
    std::string classes = get_attribute("class");
    return classes.find(className) != std::string::npos;
}

Dom *Dom::get_by_id(std::string id) {
    if (is_id(id)) {
        return this;
    }
    for(int i = 0; i < childrens.size(); ++i) {
        if (childrens[i]->is_id(id)) {
            return childrens[i];
        }
    }
    return NULL;
}

std::vector<Dom*> Dom::get_by_class(std::string className) {
    std::vector<Dom*> result;
    if (has_class(className)) {
        result.push_back(this);
    }
    for(int i = 0; i < childrens.size(); ++i) {
        if (childrens[i]->has_class(className)) {
            result.push_back(childrens[i]);
        }
    }
    return result;
}

std::vector<Dom*> Dom::get_by_tag(std::string tag) {
    std::vector<Dom*> result;
    if (get_name() == tag) {
        result.push_back(this);
    }
    for(int i = 0; i < childrens.size(); ++i) {
        if (childrens[i]->get_name() == tag) {
            result.push_back(childrens[i]);
        }
    }
    return result;
}

std::vector<Dom*> Dom::get_by_name(std::string name) {
    std::vector<Dom*> result;
    if (is_name(name)) {
        result.push_back(this);
    }
    for(int i = 0; i < childrens.size(); ++i) {
        if (childrens[i]->is_name(name)) {
            result.push_back(childrens[i]);
        }
    }
    return result;
}
